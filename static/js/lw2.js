z_eps = 1e-15;


function validateTextarea() {
    let rows = $("#input-text").val().split('\n');
    let cols_count = -1;
    let accepted_rows = 0;
    for (let ri = 0; ri < rows.length; ri++) {
        let row = rows[ri].match(/\S+/g) || [];
        if (row.length === 0) {
            continue;
        }

        if (accepted_rows === 0) {
            cols_count = row.length;
        } else  {
            if (row.length !== cols_count) {
                return 'Row ' + (ri + 1) + ': expected ' + cols_count + ' numbers, got ' + row.length +
                    ' (inconsistent row length)';
            }
        }

        for (let ci = 0; ci < row.length; ci++) {
            if (!$.isNumeric(row[ci])) {
                return 'Row ' + (ri + 1) + ', col ' + (ci + 1) + ' ("' + row[ci] + '"): not a numeric value.'
            }
        }

        if (Math.abs(row[ri]) < z_eps) {
            return  'Row ' + (ri + 1) + ', col ' + (ri + 1) +': zero value. Not allowed'
        }

        // if (row.slice(0, -1).reduce((a, b) => Math.abs(a) + Math.abs(b)) > row[ri]) {
        //     return 'Matrix is not diagonally dominant'
        // }

        accepted_rows++;
    }

    // for (let ri = 0; ri < rows.length; ri++) {
    //     let row = rows[ri].match(/\S+/g) || [];
    //
    // }

    if (cols_count === -1) return "Matrix can't be empty";
    if (cols_count > 21) return "Matrix is too big";
    if (accepted_rows !== cols_count - 1) {
        return 'Matrix is not N x N+1: ' + accepted_rows + ' rows and ' + cols_count + ' cols.'
    }

    return 'ok';
}

function updateValidation() {
    let errorbox = $("#input-error");
    errorbox.html('...');

    let res = validateTextarea();
    if (res !== 'ok') {
        errorbox.html(res);
        errorbox.removeClass('valid');
    } else {
        errorbox.html('Valid matrix');
        errorbox.addClass('valid');
    }
}

$(document).ready(function () {
    $('#file-inp').change(function () {
        let file = $(this)[0].files[0];
        let fr = new FileReader();
        fr.readAsText(file);
        fr.onload = function () {
            $('#input-text').val(fr.result);
            validateTextarea();
        };
    });

    $("#input-text").bind('input propertychange', function () {
        if (!$("#input-error").hidden){
            updateValidation();
        }
    });

    $("#input").submit(function () {
        let errorbox = $("#input-error");

        updateValidation();
        if (!errorbox.hasClass('valid')) {
            errorbox.slideDown();
            return false;
        } else {
            return true;
        }
    });

    $("#input-n-for-rand").bind('input propertychange', function () {
        $(this).val(Math.min(Math.max($(this).val(), 0), 20))
    });
});
