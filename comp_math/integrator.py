max_iterations = 100


class Func:
    def __init__(self,
                 name: str,
                 # name_latex: str,
                 f):
        self.name = name
        # self.name_latex = name_latex
        self.f = f


class Result:
    def __init__(self,
                 eps: float,
                 func: Func,
                 number_of_breaks: int,
                 result: float,
                 error: float,
                 mod: int):
        self.eps = eps
        self.func = func
        self.number_of_breaks = number_of_breaks
        self.result = result
        self.error = error
        self.mod = mod


def riemann_sum_left(f, a, b, number_of_breaks):
    step_size = (b - a) / float(number_of_breaks)
    total = sum([f((a + (k * step_size))) for k in range(0, number_of_breaks)])
    result = step_size * total
    return result


def riemann_sum_right(f, a, b, number_of_breaks):
    step_size = (b - a) / float(number_of_breaks)
    total = sum([f((a + ((k + 1) * step_size))) for k in range(0, number_of_breaks)])
    result = step_size * total
    return result


def riemann_sum_middle(f, a, b, number_of_breaks):
    step_size = (b - a) / float(number_of_breaks)
    total = sum([f((a + (k * step_size) + a + ((k + 1) * step_size)) / 2) for k in range(0, number_of_breaks)])
    result = step_size * total
    return result


def solve(func: Func, eps: float, upper_limit: float, lower_limit: float, mod: int) -> Result or str:
    riemann_sum = riemann_sum_left
    if mod == 1:
        riemann_sum = riemann_sum_right
    elif mod == 2:
        riemann_sum = riemann_sum_middle

    ans = [0, 0]
    number_of_breaks = 2
    ans[0] = riemann_sum(func.f, lower_limit, upper_limit, number_of_breaks)
    number_of_breaks *= 2
    ans[1] = riemann_sum(func.f, lower_limit, upper_limit, number_of_breaks)
    i = 0
    while abs(ans[0] - ans[1]) / 3 > eps and i < max_iterations:
        number_of_breaks *= 2
        ans[i % 2] = riemann_sum(func.f, lower_limit, upper_limit, number_of_breaks)
        i += 1

    return Result(eps, func, number_of_breaks, ans[(i - 1) % 2], abs(ans[0] - ans[1]), mod)
