from flask import Flask, render_template, request
from comp_math.integrator import solve, Func
import math

app = Flask(__name__)

functions = [Func("sin(x)", lambda x: math.sin(x)),
             Func("cos(x)", lambda x: math.cos(x)),
             Func("x^2", lambda x: x ** 2),
             Func("x^3-8*x^2+10*x", lambda x: x**3-8*x**2+10*x)]


@app.route('/', methods=['GET'])
def index():
    return render_template("lw2.html", functions=functions)


@app.route('/', methods=['POST'])
def lab2_post():
    func = int(request.form['func'])
    upper_limit = float(request.form['upper_limit'])
    lower_limit = float(request.form['lower_limit'])
    eps = float(request.form['eps'])
    mod = int(request.form['mod'])
    if mod > 2 or mod < 0:
        mod = 2

    result = solve(functions[func], eps, upper_limit, lower_limit, mod)
    if type(result) == str:
        return result

    return render_template("lw2.html", output=result, functions=functions, upper_limit=upper_limit, lower_limit=lower_limit)


if __name__ == '__main__':
    app.run()
